import { getDataByYear } from '~/services/dollarDataService'
export const state = () => ({
  dollarPrices: [],
})

export const mutations = {
  setDollarData(state, payload) {
    Object.assign(state.dollarPrices, payload.dollarPrices)
  },
}

export const actions = {
  async saveDollarDataByYear({ commit }, payload) {
    const previousYear = await getDataByYear(
      payload.dollarType,
      payload.fromYear
    )
    const currentYear = await getDataByYear(payload.dollarType, payload.toYear)
    const dollarPrices = [
      ...previousYear.data.serie.reverse(),
      ...currentYear.data.serie.reverse(),
    ]
    commit('setDollarData', { dollarPrices })
  },
}

export const getters = {
  dollarPricesByYear: (state) => {
    return state.dollarPrices
  },
}
